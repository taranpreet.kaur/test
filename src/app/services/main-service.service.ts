import { Injectable } from '@angular/core';
import { CrudService } from './crud.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MainServiceService {

  constructor(private crudService: CrudService) { }

  adminLogin(body: any): Observable<string> {
    var fd = new FormData();
    fd.append('email', body.email);
    fd.append('password', body.password);
    return this.crudService.post<any>('extranet/login', fd);
  }

  saveData(body: any): Observable<any> {
    const fd = new FormData();
    fd.append('offerName', body.offerName);
    fd.append('offerDesc', body.offerDesc);
    fd.append('offerPrice', body.offerPrice);
    fd.append('currencyType', body.currencyType);
    fd.append('link', body.link);
    fd.append('event', body.event);
    for (var i = 0; i < body.offerIcon.length; i++) {
      fd.append('offerAttachments', body.offerIcon[i]);
    }
    return this.crudService.post('extranet/createOffer', fd);
  }

  editData(body: any): Observable<any> {
    const fd = new FormData();
    fd.append('offerName', body.offerName);
    fd.append('offerDesc', body.offerDesc);
    fd.append('offerPrice', body.offerPrice);
    fd.append('currencyType', body.currencyType);
    fd.append('link', body.link);
    fd.append('offerId', body._id)
    return this.crudService.post('extranet/editOffer', fd);
  }

  getData(): Observable<any> {
    return this.crudService.get('extranet/getOffers');
  }

  getUserDetails(): Observable<any> {
    const fd = new FormData();
    return this.crudService.post('app/userDetails', fd);
  }

  getDeleteRow(id: any) {
    const fd = new FormData();
    debugger;
    fd.append('offerId', id);
    return this.crudService.post('extranet/deleteOffer', fd);
  }
}
