import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  siteUrl: string = 'http://65.2.148.81:8040/api/'; // API URL

  constructor(private http: HttpClient) { }

  get<T>(url): Observable<T> {
    return this.http.get<T>(this.siteUrl + url);
  }

  getWithHeaders(url, headers): any {
    return this.http.get(this.siteUrl + url, headers);
  }

  post<T>(url, body): any {
    return this.http.post<T>(this.siteUrl + url, body);
  }

  postWithHeader(url, body, headers): any {
    return this.http.post(this.siteUrl + url, body, headers);
  }

  put<T>(url, body): any {
    return this.http.put<T>(this.siteUrl + url, body);
  }

  delete() {
  }
}
