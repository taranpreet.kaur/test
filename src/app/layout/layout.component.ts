import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  userName: any = ''
  constructor(private router: Router) {
    this.userName = localStorage.getItem('userName');
  }

  ngOnInit() {
  }

}
