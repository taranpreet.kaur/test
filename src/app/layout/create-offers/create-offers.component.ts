import { Component, OnInit } from '@angular/core';
import { Offers } from 'src/app/model/offer.model';
import { MainServiceService } from 'src/app/services/main-service.service';


@Component({
  selector: 'app-create-offers',
  templateUrl: './create-offers.component.html',
  styleUrls: ['./create-offers.component.css']
})
export class CreateOffersComponent implements OnInit {
  createOffers: Offers;
  rowData = [];

  constructor(private mainService: MainServiceService) {
    this.createOffers = new Offers();
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.mainService.getData().subscribe((data) => {
      this.rowData = data.offers;
    },
      (error) => {
        console.log(error.error.message);
      });
  }

  validation() {
    if (this.createOffers.offerName == '' || this.createOffers.offerName == undefined || this.createOffers.offerName == null) {
      alert('Please select name');
      return true;
    }
    if (this.createOffers.offerDesc == '' || this.createOffers.offerDesc == undefined || this.createOffers.offerDesc == null) {
      alert('Please select Offer Description');
      return true;
    }
    if (this.createOffers.offerPrice == '' || this.createOffers.offerPrice == undefined || this.createOffers.offerPrice == null) {
      alert('Please select Offer Price');
      return true;
    }
    if (this.createOffers.currencyType == '' || this.createOffers.currencyType == undefined || this.createOffers.currencyType == null) {
      alert('Please select Currency Type');
      return true;
    }
    if (this.createOffers.link == '' || this.createOffers.link == undefined || this.createOffers.link == null) {
      alert('Please enter link');
      return true;
    }
  }

  create() {
    if (this.validation()) {
      return;
    }
    this.mainService.saveData(this.createOffers).subscribe((data: any) => {
      this.getData();
      alert('data save successfully');
      this.reset();
    },
      (error) => {
        console.log(error.message);
      });
  }

  edit() {
    if (this.validation()) {
      return;
    }
    this.mainService.editData(this.createOffers).subscribe((data: any) => {
      console.log(data);
      this.getData();
      alert('Data updated successfully');
      this.reset();
    },
      (error) => {
        console.log(error.message);
      });
  }

  onFileChange(event: any) {
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      let files = [];
      files.push(file);
      this.createOffers.offerIcon = files;
    };
  }

  reset() {
    this.createOffers = new Offers();
    this.hideShow = true;
    (<HTMLFormElement>document.getElementById("offers")).reset();
  }

  onDelete(model: Offers) {
    if (confirm("Are you sure?, You want to delete this ?")) {
      console.log(model);
      this.mainService.getDeleteRow(model._id).subscribe(
        data => {
          this.getData();
          alert('data deleted successfully');
        },
        error => { console.error(); }
      );
    }
  }

  hideShow: boolean = true;
  EditAppMaster(item: any) {
    this.hideShow = false;
    this.createOffers = new Offers();
    this.createOffers._id = item["_id"];
    this.createOffers.currencyType = item["currencyType"];
    this.createOffers.link = item["link"];
    this.createOffers.offerDesc = item["offerDesc"];
    this.createOffers.offerPrice = item["offerPrice"];
    this.createOffers.offerName = item["offerName"];
  }


}
