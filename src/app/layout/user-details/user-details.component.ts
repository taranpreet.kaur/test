import { Component, OnInit } from '@angular/core';
import { MainServiceService } from 'src/app/services/main-service.service';
import { MasterFilterPipe } from 'src/app/layout/user-details/filter'

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  rowData = [];
  excelData: any[];
  searchTerm: string;
  MasterFilterPipe: any;
  constructor(private mainService: MainServiceService) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.mainService.getUserDetails().subscribe((data) => {
      this.rowData = data;
      this.excelData = data;
    },
      (error) => {
        console.log(error.message);
      });
  }

  //#region  - Vinay
  //#region Export to excel
  CreateHTMLTable() {
    var tab_text = `<table border="1px" style="font-size:14px">
 <thead>
 <tr style="background-color: yellow;font-weight: bold;">
 <th>Name</th>
<th>Email</th>
<th>Phone Number</th>
<th>Created On</th>
<th>WithDraw Status</th>
<th>WithDraw Date</th>
 </tr></thead><tbody>`;

    for (var j = 0; j < this.excelData.length; j++) {
      tab_text += `<tr>
      <td>`+ this.excelData[j].name + `</td>
      <td>`+ this.excelData[j].email + `</td>
      <td>`+ this.excelData[j].phoneNumber + `</td>
      <td>`+ this.excelData[j].createdAt + `</td>
      <td>`+ this.excelData[j].withdrawRequest + `</td>
      <td>`+ this.excelData[j].withdrawStandTime + `</td>
</tr>
 `;
    }
    tab_text += '</tbody> <table';
    return tab_text;
  }

  GenerateExcel() {
    var table = this.CreateHTMLTable();
    var element = document.createElement('a');
    element.setAttribute('href', 'data:application/vnd.ms-excel,' + encodeURIComponent(table));
    element.setAttribute('download', 'Offer' + ".xls");
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }
  base64_encode(data) {
    var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
      ac = 0,
      enc = "",
      tmp_arr = [];

    if (!data) {
      return data;
    }

    do { // pack three octets into four hexets
      o1 = data.charCodeAt(i++);
      o2 = data.charCodeAt(i++);
      o3 = data.charCodeAt(i++);

      bits = o1 << 16 | o2 << 8 | o3;

      h1 = bits >> 18 & 0x3f;
      h2 = bits >> 12 & 0x3f;
      h3 = bits >> 6 & 0x3f;
      h4 = bits & 0x3f;

      // use hexets to index into b64, and append result to encoded string
      tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
    } while (i < data.length);

    enc = tmp_arr.join('');

    var r = data.length % 3;

    return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);

  }
  //#endregion


}
//Vinaysingh@123---way2vinaypratap