import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'masterFilter'
})


export class MasterFilterPipe implements PipeTransform {
    transform(Masterlist: any[], searchTerm: string): any[] {
        if (!Masterlist || !searchTerm) {
            return Masterlist
        }
        return Masterlist.filter(master =>
            master.name.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1 ||
            master.createdAt.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1 ||
            master.withdrawRequest.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1 ||
            master.email.toString().indexOf(searchTerm.toString()) !== -1
        );
    }
}