import { Component, OnInit } from '@angular/core';
import { MainServiceService } from '../services/main-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})


export class LoginComponent implements OnInit {
  isLoginError = false;
  email: any;
  otp: any;
  password: any;
  ConfirmPass: any = '';
  //fPassword : any = '';

  constructor(public titleService: Title,
    private router: Router, private mainService: MainServiceService,
    private activatedRoute: ActivatedRoute) {
    this.titleService.setTitle("Login");
    this.loginForm = new FormGroup({
      email: new FormControl(null),
      password: new FormControl(null)
    });
  }

  loginForm: FormGroup;

  emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  serverErrorMessages: string;
  user: any = '';
  loggedIn: boolean;

  ngOnInit() {
  }

  isValid(controlName) {
    return this.loginForm.get(controlName).invalid && this.loginForm.get(controlName).touched;
  }

  submitted: boolean = false;



  adminLogin() {
    this.submitted = true;
    if (this.loginForm.valid) {
      this.mainService.adminLogin(this.loginForm.value)
        .subscribe(
          (data: any) => {
            console.log(data);

            if (data.token && data.token !== '') {
              {
                localStorage.setItem('userToken', data.token);
                localStorage.setItem('userId', data.user._id);
                localStorage.setItem('userName', data.user.name);
                localStorage.setItem('phoneNumber', data.user.phoneNumber);
                localStorage.setItem('email', data.user.email);
                this.router.navigate(['/dashboard'], { replaceUrl: true });
              }
            }
            else if (data.message == "Incorrect Password") {
              alert('You have entered incorrect password');
            }
            else {
              alert('You are not a registered user');
            }
          },
          error => {
            this.isLoginError = error;
            alert(error);
          }
        );
    }
  }

  InfoDialog: boolean;
  isOtp: boolean = false;
  resetPassword: boolean = false;
  emailid: any = false;

  showDialog() {
    this.InfoDialog = true;
    this.isOtp = false;
    this.resetPassword = false;
    this.emailid = false;
    this.password = "";
    this.ConfirmPass = "";
    this.otp = "";
    this.email = "";
  }

  ValidatePassword() {
    var pass = this.password;
    if (this.password == undefined || this.password == '') {
      alert('Please enter Password');
      return false;
    }

    if (pass.length >= 8 && pass.length <= 20) {
      var regularExpression = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/;
      var letterSmall = /[a-z]/;
      var letterCaps = /[A-Z]/;
      var number = /[0-9]/;
      var special = /[.*@]/;
      var valid = letterSmall.test(pass) && letterCaps.test(pass) && number.test(pass) && special.test(pass)//match a letter _and_ a number

      if (!valid) {
        alert("Password must contain at least 1 UpperCase Alphabet, 1 Lowercase Alphabet, 1 Number, 1 Special Character");
        return false;
      }
    }
    else {
      alert('Password must contain Minimum 8 and Maximum 20 characters');
      return false;
    }

    if (this.ConfirmPass == undefined || this.ConfirmPass == '') {
      alert('Please enter Confirm Password');
      return false;
    }

    if (this.password != this.ConfirmPass) {
      alert("New Password & Confirm Password Doesn't match.");
      return false;
    }

    return true;
  }


}

