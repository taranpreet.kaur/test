import { RouterModule, Routes, Route } from '@angular/router';
import { CreateOffersComponent } from './layout/create-offers/create-offers.component';
import { LayoutComponent } from './layout/layout.component';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { UserDetailsComponent } from './layout/user-details/user-details.component';


const appRoutes: Routes = [
  { path: "", redirectTo: "/login", pathMatch: "full" },
  { path: "login", component: LoginComponent },
  {
    path: 'dashboard', component: LayoutComponent,
    children: [{ path: '', component: CreateOffersComponent }]
  },
  {
    path: 'userdetails', component: LayoutComponent,
    children: [{ path: '', component: UserDetailsComponent }]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }