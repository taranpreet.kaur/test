import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';

import { CreateOffersComponent } from './layout/create-offers/create-offers.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutComponent } from './layout/layout.component';
import { AppRoutingModule } from './app-routing.module';
import { UserDetailsComponent } from './layout/user-details/user-details.component';
import { MasterFilterPipe } from "../app/layout/user-details/filter";


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CreateOffersComponent,
    LayoutComponent,
    UserDetailsComponent,
    MasterFilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
